import java.util.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean continuar=true;
		while(continuar){
			Shape s;
			System.out.println("�qu� figura desea crear?\n\t 1.Triangulo. \t 2.Rectangulo.");
			java.util.Scanner input = new java.util.Scanner(System.in);
			try{
				int eleccion=input.nextInt();
				System.out.println("Ingrese la altura.");
				double h=input.nextDouble();
				System.out.println("Ingrese la base.");
				double w=input.nextDouble();
				switch(eleccion){
				case 1:
					s= new Triangle(w,h);
					System.out.printf("Triangulo  Area= %.2f Perimetro=%.2f %n",s.getArea(),s.getPerimeter());
					break;
				case 2:
					s= new Rectangle(w,h);
					System.out.printf("Rectangulo  Area= %.2f Perimetro=%.2f",s.getArea(),s.getPerimeter());
					break;
				default:
					System.out.println("Ingrese una opci�n v�lida.");
				}
				System.out.println("Desea crear otra figura? \n\t 1.Si.\t 2.No.");
				eleccion=input.nextInt();
				if(eleccion==2){
					continuar=false;
					System.out.println("Sesi�n finalizada.");
				}
			}catch(InputMismatchException e){
				System.out.println("Ingrese los datos correctamente, tenga en cuenta que para separar los decimales de utilizar una coma");
			}catch(IllegalArgumentException e){
				System.out.println("La base y la altura deben ser positivos.");
			}
		}
	}
}


