
public class Triangle extends Shape {

	public Triangle(double width, double height) {
		super(3);
		this.height=height;
		this.width=width;
		if(height<=0 || width<=0){
			throw new IllegalArgumentException("La base y la altura deben ser ser positivos.");
		}
	}
	
	@Override
	public double getArea() {
		return 0.5*this.getHeight()*this.getWidth();
	}

	@Override
	public double getPerimeter() {
		//suponiendo un triangulo rectángulo
		return this.getHeight()+this.getWidth()+Math.sqrt(Math.pow(this.getArea(), 2)+Math.pow(this.getHeight(), 2));
	}

}
