
public abstract class Shape {
	int numSides;
	double width;
	double height;
	
	public double getWidth(){
		return this.width;
	}
	
	public double getHeight(){
		return this.height;
	}
	public Shape(int numSides){
		this.numSides=numSides;
	}

	public int getNumSides(){
		return this.numSides;
	}

	public abstract double getArea();
	public abstract double getPerimeter();
}
