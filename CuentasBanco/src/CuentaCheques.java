
public class CuentaCheques extends Cuenta {
	private double interesChequera;
	private double interesSaldoInsuficiente;

	public CuentaCheques(String numCuenta, String nombreCliente, double saldo, double interesChequera, double interesSaldoInsuficiente) {
		super(numCuenta, nombreCliente, saldo);
		this.interesChequera=interesChequera;
		this.interesSaldoInsuficiente=interesSaldoInsuficiente;
	}

	public double getInteresChequera(){
		return this.interesChequera;
	}
	
	public double getInteresSalodInsuficiente(){
		return this.interesSaldoInsuficiente;
	}
	
	public void retirar(double valorCheque){
		if(valorCheque*(1+this.getInteresChequera())<this.getSaldo()){
			this.saldo-=valorCheque*(1+this.getInteresChequera());
		}else{
			this.saldo+=valorCheque*(1+this.getInteresChequera()+this.getInteresSalodInsuficiente());
		}
		System.out.println("Operaci�n exitosa, su saldo restante es :"+this.getSaldo());
	}
	
	public void consultar(){
		super.consultar();
		System.out.println("\t Interes por uso de Chequera: "+this.getInteresChequera()+
				"\n \t Inter�s por emisi�n de cheques con saldo insuficiente: "+this.getInteresSalodInsuficiente());
	}
}
