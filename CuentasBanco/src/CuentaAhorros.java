import java.util.*;

public class CuentaAhorros extends Cuenta {
	private Date fechaVencimiento;
	private double interesMensual;

	public CuentaAhorros(String numCuenta, String nombreCliente, double saldo,Date fechaVencimiento,double interesMensual) {
		super(numCuenta, nombreCliente, saldo);
		this.fechaVencimiento=fechaVencimiento;
		this.interesMensual=interesMensual;
	}
	
	public Date getFechaVencimiento(){
		return this.fechaVencimiento;
	}
	
	public double getInteresMensual(){
		return this.interesMensual;
	}
	
	public void consultar(){
		super.consultar();
		System.out.println("\t Fecha de Vencimiento: "+this.getFechaVencimiento().toString()+
				"\n \t Interes Mensual: "+this.getInteresMensual());
	}

	public void retirarAhorros(Date fechaRetiro){
		if(fechaRetiro.equals(this.getFechaVencimiento())){
			this.saldo=0;
			System.out.println("Operaci�n exitosa, su saldo restante es "+this.getSaldo());
		}else{
			System.out.println("S�lo se le permite retirar sus ahorros en la fecha de vencimiento de la cuenta.");
		}
	}
	
}